
(async function(){
let data=0;
let url='';
let allUrl=[];
for(let i=1; i<=31;i++) {
    data = '202001' + i;
    if (i < 10) {
        i = '0' + i;
        data = '202001' + i;
    }
    url=`https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=${data}&json`;
    allUrl.push(url);
};

    let requests = allUrl.map(url1 => fetch(url1).then(res => res.json()));
    const resultX =await Promise.all(requests);


    let USD=resultX.map(el=>{
        return{
            'currency':el[26].cc,
            'date':el[26].exchangedate,
            'rate':el[26].rate,
        }
    });


    let EUR=resultX.map(el=>{
        return{
            'currency':el[33].cc,
            'date':el[33].exchangedate,
            'rate':el[33].rate,
        }
    });

let dateEUR=EUR.map(el=>el.date);
let rateEUR=EUR.map(el=>el.rate);
let rateUSD=USD.map(el=>el.rate);

var ctx = document.getElementById('myChart').getContext('2d');
    var chart=new Chart(ctx, {
    type: 'line',
    data: {
        labels: dateEUR,
        datasets: [{
            label: 'EUR',
            backgroundColor: 'white',
            borderColor: 'blue',
            data: rateEUR,
        },
            {
                label: 'USD',
                backgroundColor: 'white',
                borderColor: 'green',
                data: rateUSD,
            }]
    },
    options: {}
});


})();

